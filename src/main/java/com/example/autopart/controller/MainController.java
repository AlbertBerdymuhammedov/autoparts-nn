package com.example.autopart.controller;

import com.example.autopart.commands.*;
import com.example.autopart.converters.*;
import com.example.autopart.entity.*;
import com.example.autopart.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class MainController {

    private CustomerService customerService;
    private CustomerToCustomerForm customerToCustomerForm;

    private ArchiveService archiveService;
    private ArchiveToArchiveForm archiveToArchiveForm;

    private OrderService orderService;
    private OrderToOrderForm orderToOrderForm;

    private StaffService staffService;
    private StaffToStaffForm staffToStaffForm;

    private MarkService markService;
    private DetailService detailService;
    private NameprtService nameprtService;
    private StatusService statusService;

    @Autowired
    public void setCustomerService(CustomerService customerService){ this.customerService = customerService;}
    @Autowired
    public void setCustomerToCustomerForm(CustomerToCustomerForm customerToCustomerForm){this.customerToCustomerForm=customerToCustomerForm;}

    @Autowired
    public void setOrderService(ArchiveService archiveService){this.archiveService = archiveService;}
    @Autowired
    public void setArchiveToArchiveForm(ArchiveToArchiveForm archiveToArchiveForm){this.archiveToArchiveForm = archiveToArchiveForm;}

    @Autowired
    public void setOrderService(OrderService orderService){this.orderService = orderService;}
    @Autowired
    public void setOrderToOrderForm(OrderToOrderForm orderToOrderForm){this.orderToOrderForm = orderToOrderForm;}

    @Autowired
    public void setStaffService(StaffService staffService){ this.staffService = staffService;}
    @Autowired
    public void setStaffToStaffForm(StaffToStaffForm staffToStaffForm){ this.staffToStaffForm = staffToStaffForm;}

    @Autowired
    public void setMarkService(MarkService markService) {
        this.markService = markService;
    }

    @Autowired
    public void setDetailService(DetailService detailService) {
        this.detailService = detailService;
    }

    @Autowired
    public void setNameprtService(NameprtService nameprtService) {
        this.nameprtService = nameprtService;
    }

    @Autowired
    public void setStatusService(StatusService statusService) {
        this.statusService = statusService;
    }

    
    //region Archives

    @RequestMapping({"/archives/TableArchive", "/archives"})
    public String TableArchive(Model model){
        model.addAttribute("archives", archiveService.listAll());
        model.addAttribute("orders", orderService.listAll());
        model.addAttribute("customers", customerService.listAll());
        model.addAttribute("staffs", staffService.listAll());
        return "/archive/TableArchive";
    }

    @RequestMapping("/archives/edit/{id}")
    public String EditArchive(@PathVariable String id, Model model){
        Archives archives = archiveService.getById(Long.valueOf(id));
        ArchiveForm archiveForm = archiveToArchiveForm.convert(archives);

        model.addAttribute("archiveForm", archiveForm);
        model.addAttribute("orders", orderService.listAll());
        model.addAttribute("customers", customerService.listAll());
        model.addAttribute("staffs", staffService.listAll());
        return "/archive/EditArchive";
    }

    @RequestMapping(value = "/archives", method = RequestMethod.POST)
    public String saveOrUpdateArchive(@Valid ArchiveForm archiveForm, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "/archives/EditArchive";
        }

        archiveService.saveOrUpdateArchiveForm(archiveForm);

        return "redirect:/archives/TableArchive";
    }

    @RequestMapping("/archives/delete/{id}")
    public String deleteArchive(@PathVariable String id){
        archiveService.delete(Long.valueOf(id));
        return "redirect:/archives/TableArchive";
    }

    @RequestMapping("/archives/new")
    public String newArchive(Model model){
        model.addAttribute("archiveForm", new ArchiveForm());
        model.addAttribute("orders", orderService.listAll());
        model.addAttribute("customers",customerService.listAll());
        model.addAttribute("staffs", staffService.listAll());
        return "/archive/EditArchive";
    }
    //endregion

    //region Orders

    @RequestMapping({"/orders/TableOrder", "/orders"})
    public String TableOrder(Model model){
        model.addAttribute("orders", orderService.listAll());
        model.addAttribute("status", statusService.listAll());
        model.addAttribute("details", detailService.listAll());
        model.addAttribute("markautos", markService.listAll());
        model.addAttribute("nameparts", nameprtService.listAll());
        model.addAttribute("staffs", staffService.listAll());
        return "/order/TableOrder";
    }

    @RequestMapping("/orders/show/{id}")
    public String getOrder(@PathVariable String id, Model model){
        model.addAttribute("orders", orderService.getById(Long.valueOf(id)));
        model.addAttribute("status", statusService.listAll());
        model.addAttribute("details", detailService.listAll());
        model.addAttribute("staffs",staffService.listAll());
        model.addAttribute("markautos", markService.listAll());
        model.addAttribute("nameparts", nameprtService.listAll());
        return "/order/ShowOrder";
    }

    @RequestMapping("/orders/edit/{id}")
    public String EditOrder(@PathVariable String id, Model model){
        Orders orders = orderService.getById(Long.valueOf(id));
        OrderForm orderForm = orderToOrderForm.convert(orders);

        model.addAttribute("orderForm", orderForm);
        model.addAttribute("status", statusService.listAll());
        model.addAttribute("staffs", staffService.listAll());
        return "/order/EditOrder";
    }

    @RequestMapping(value = "/orders", method = RequestMethod.POST)
    public String saveOrUpdateOrders(@Valid OrderForm orderForm, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "/orders/EditOrder";
        }

        orderService.saveOrUpdateOrderForm(orderForm);

        return "redirect:/orders/TableOrder";
    }

    @RequestMapping("/orders/delete/{id}")
    public String deleteOrder(@PathVariable String id){
        orderService.delete(Long.valueOf(id));
        return "redirect:/orders/TableOrder";
    }
    //endregion

    //region Redir

    @RequestMapping("/order")
    public String redirToOrder(){
        return "redirect:/";
    }

    @RequestMapping("/customer")
    public String redirToCustomer(){
        return "redirect:/";
    }

    @RequestMapping("/staff")
    public String redirToStaff(){
        return "redirect:/";
    }

    @RequestMapping("/archive")
    public String redirToArchive(){
        return "redirect:/";
    }
    //endregion

    //region Home

    @RequestMapping(value = "/", produces = { MediaType.TEXT_HTML_VALUE })
    public String Home(Model model)
    {
        model.addAttribute("orderForm", new OrderForm());
        model.addAttribute("customerForm", new CustomerForm());
        model.addAttribute("status", statusService.listAll());
        model.addAttribute("details", detailService.listAll());
        model.addAttribute("markautos", markService.listAll());
        model.addAttribute("staffs", staffService.listAll());
        model.addAttribute("nameparts", nameprtService.listAll());
        return "/home";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String showOrdersByTrack(@RequestParam (value = "search", required = false) String track, Model model) {
        model.addAttribute("customerForm", new CustomerForm());
        model.addAttribute("orderForm", new OrderForm());
        model.addAttribute("orders", orderService.listAll());
        model.addAttribute("status", statusService.listAll());
        model.addAttribute("details", detailService.listAll());
        model.addAttribute("markautos", markService.listAll());
        model.addAttribute("nameparts", nameprtService.listAll());
        model.addAttribute("staffs", staffService.listAll());
        model.addAttribute("searchResult", orderService.listByTrack(track));
        return "home";
    }

    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    public String saveOrUpdateCustomer(@Valid CustomerForm customerForm, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "/home";
        }

        customerService.saveOrUpdateCustomerForm(customerForm);

        return "redirect:/#zatemnenie1";
    }

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    public String saveOrUpdateOrder(@Valid OrderForm orderForm, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "/home";
        }

        orderService.saveOrUpdateOrderForm(orderForm);

        return "redirect:/#zatemnenie2";
    }
    //endregion

    //region Customers

    @RequestMapping({"/customers/TableCus", "/customers"})
    public String TableCustomer(Model model){
        model.addAttribute("customers", customerService.listAll());
        return "/customer/TableCus";
    }

    @RequestMapping(value = "/customers", method = RequestMethod.POST)
    public String saveOrUpdateCustomers(@Valid CustomerForm customerForm, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "/customers/EditCus";
        }

        Customers savedCustomers = customerService.saveOrUpdateCustomerForm(customerForm);

        return "redirect:/customers/TableCus";
    }

    @RequestMapping("/customers/delete/{id}")
    public String deleteCustomer(@PathVariable String id){
        customerService.delete(Long.valueOf(id));
        return "redirect:/customers/TableCus";
    }

    @RequestMapping("/customers/edit/{id}")
    public String EditCustomer(@PathVariable String id, Model model){
        Customers customers = customerService.getById(Long.valueOf(id));
        CustomerForm customerForm = customerToCustomerForm.convert(customers);

        model.addAttribute("customerForm", customerForm);
        return "/customer/EditCus";
    }
    //endregion

    //region Staffs

    @RequestMapping({"/staffs/TableStaff", "/staffs"})
    public String TableStaff(Model model){
        model.addAttribute("staffs", staffService.listAll());
        return "/staff/TableStaff";
    }

    @RequestMapping("/staffs/show/{id}")
    public String getStaff(@PathVariable String id, Model model){
        model.addAttribute("staffs", staffService.getById(Long.valueOf(id)));
        return "/staff/ShowStaff";
    }

    @RequestMapping("/staffs/edit/{id}")
    public String EditStaff(@PathVariable String id, Model model){
        Staffs staffs = staffService.getById(Long.valueOf(id));
        StaffForm staffForm = staffToStaffForm.convert(staffs);

        model.addAttribute("staffForm", staffForm);
        return "/staff/EditStaff";
    }

    @RequestMapping(value = "/staffs", method = RequestMethod.POST)
    public String saveOrUpdateStaff(@Valid StaffForm staffForm, BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            return "/staffs/EditStaff";
        }

        staffService.saveOrUpdateStaffForm(staffForm);

        return "redirect:/staffs/TableStaff";
    }

    @RequestMapping("/staffs/delete/{id}")
    public String deleteStaff(@PathVariable String id){
        staffService.delete(Long.valueOf(id));
        return "redirect:/staffs/TableStaff";
    }

    @RequestMapping("/staffs/new")
    public String newStaff(Model model){
        model.addAttribute("staffForm", new StaffForm());
        return "/staff/EditStaff";
    }
    //endregion


}
