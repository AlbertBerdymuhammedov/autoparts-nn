package com.example.autopart.repository;

import com.example.autopart.entity.Orders;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepo extends CrudRepository<Orders, Long> {
    Iterable<Orders> findByTrack(String track);
}
