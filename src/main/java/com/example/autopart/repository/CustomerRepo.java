package com.example.autopart.repository;

import com.example.autopart.entity.Customers;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepo extends CrudRepository<Customers, Long> {
}
