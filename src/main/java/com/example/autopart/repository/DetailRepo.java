package com.example.autopart.repository;

import com.example.autopart.entity.Details;
import org.springframework.data.repository.CrudRepository;

public interface DetailRepo extends CrudRepository<Details, Long> {
}
