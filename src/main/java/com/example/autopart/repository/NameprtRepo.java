package com.example.autopart.repository;

import com.example.autopart.entity.Nameparts;
import org.springframework.data.repository.CrudRepository;

public interface NameprtRepo extends CrudRepository<Nameparts, Long> {
}
