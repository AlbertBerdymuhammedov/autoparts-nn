package com.example.autopart.repository;

import com.example.autopart.entity.Archives;
import org.springframework.data.repository.CrudRepository;

public interface ArchiveRepo extends CrudRepository<Archives, Long> {
}
