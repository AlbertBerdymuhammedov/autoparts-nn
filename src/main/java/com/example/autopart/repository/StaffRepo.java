package com.example.autopart.repository;

import com.example.autopart.entity.Staffs;
import org.springframework.data.repository.CrudRepository;

public interface StaffRepo extends CrudRepository<Staffs, Long> {
}
