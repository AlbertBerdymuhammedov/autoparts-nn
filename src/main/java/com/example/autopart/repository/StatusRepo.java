package com.example.autopart.repository;

import com.example.autopart.entity.Status;
import org.springframework.data.repository.CrudRepository;

public interface StatusRepo extends CrudRepository<Status,Long> {
}
