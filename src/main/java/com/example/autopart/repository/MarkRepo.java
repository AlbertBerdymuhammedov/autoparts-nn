package com.example.autopart.repository;

import com.example.autopart.entity.Markautos;
import org.springframework.data.repository.CrudRepository;

public interface MarkRepo extends CrudRepository<Markautos, Long> {
}
