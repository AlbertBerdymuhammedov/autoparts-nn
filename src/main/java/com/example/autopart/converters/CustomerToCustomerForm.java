package com.example.autopart.converters;

import com.example.autopart.commands.CustomerForm;
import com.example.autopart.entity.Customers;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerToCustomerForm implements Converter<Customers, CustomerForm> {
    @Override
    public CustomerForm convert(Customers customers){
        CustomerForm customerForm = new CustomerForm();
        customerForm.setId(customers.getId());
        customerForm.setAdress(customers.getAdress());
        customerForm.setFullname(customers.getFullname());
        customerForm.setPhone(customers.getPhone());
        return customerForm;
    }
}
