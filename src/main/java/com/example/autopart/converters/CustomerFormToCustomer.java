package com.example.autopart.converters;

import com.example.autopart.commands.CustomerForm;
import com.example.autopart.entity.Customers;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class CustomerFormToCustomer implements Converter<CustomerForm, Customers> {
    @Override
    public Customers convert(CustomerForm customerForm){
        Customers customers = new Customers();
        if (customerForm.getId() != null  && !StringUtils.isEmpty(customerForm.getId())) {
            customers.setId(customerForm.getId());
        }
        customers.setAdress(customerForm.getAdress());
        customers.setFullname(customerForm.getFullname());
        customers.setPhone(customerForm.getPhone());
        return customers;
    }
}
