package com.example.autopart.converters;

import com.example.autopart.commands.StaffForm;
import com.example.autopart.entity.Staffs;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class StaffFormToStaff implements Converter<StaffForm, Staffs> {
    @Override
    public Staffs convert(StaffForm staffForm){
        Staffs staffs = new Staffs();
        if (staffForm.getId() != null  && !StringUtils.isEmpty(staffForm.getId())) {
            staffs.setId(staffForm.getId());
        }
        staffs.setFullname(staffForm.getFullname());
        staffs.setPositions(staffForm.getPositions());
        staffs.setPhone(staffForm.getPhone());
        return staffs;
    }
}
