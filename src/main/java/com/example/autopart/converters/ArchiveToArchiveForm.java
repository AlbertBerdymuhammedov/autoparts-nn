package com.example.autopart.converters;

import com.example.autopart.commands.ArchiveForm;
import com.example.autopart.entity.Archives;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ArchiveToArchiveForm implements Converter<Archives, ArchiveForm> {
    @Override
    public ArchiveForm convert(Archives archives){
        ArchiveForm archiveForm = new ArchiveForm();
        archiveForm.setId(archives.getId());
        archiveForm.setCustomerid(archives.getCustomerid());
        archiveForm.setStaffid(archives.getStaffid());
        archiveForm.setDate(archives.getDate());
        archiveForm.setPartid(archives.getPartid());
        return archiveForm;
    }
}
