package com.example.autopart.converters;

import com.example.autopart.commands.StaffForm;
import com.example.autopart.entity.Staffs;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StaffToStaffForm implements Converter<Staffs, StaffForm> {
    @Override
    public StaffForm convert(Staffs staffs){
        StaffForm staffForm = new StaffForm();
        staffForm.setId(staffs.getId());
        staffForm.setFullname(staffs.getFullname());
        staffForm.setPositions(staffs.getPositions());
        staffForm.setPhone(staffs.getPhone());
        return staffForm;
    }
}
