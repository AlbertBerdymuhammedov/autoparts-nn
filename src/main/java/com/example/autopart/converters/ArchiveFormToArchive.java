package com.example.autopart.converters;

import com.example.autopart.commands.ArchiveForm;
import com.example.autopart.entity.Archives;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ArchiveFormToArchive implements Converter<ArchiveForm, Archives> {
    @Override
    public Archives convert(ArchiveForm archiveForm){
        Archives archives = new Archives();
        if (archiveForm.getId() != null  && !StringUtils.isEmpty(archiveForm.getId())) {
            archives.setId(archiveForm.getId());
        }
        archives.setCustomerid(archiveForm.getCustomerid());
        archives.setStaffid(archiveForm.getStaffid());
        archives.setDate(archiveForm.getDate());
        archives.setPartid(archiveForm.getPartid());
        return archives;
    }

}
