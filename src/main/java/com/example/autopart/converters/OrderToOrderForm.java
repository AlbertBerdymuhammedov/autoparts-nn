package com.example.autopart.converters;

import com.example.autopart.commands.OrderForm;
import com.example.autopart.entity.Orders;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class OrderToOrderForm implements Converter<Orders, OrderForm> {
    @Override
    public OrderForm convert(Orders orders){
        OrderForm orderForm = new OrderForm();
        orderForm.setId(orders.getId());
        orderForm.setDetailid(orders.getDetailid());
        orderForm.setMarkid(orders.getMarkid());
        orderForm.setPriceid(orders.getPriceid());
        orderForm.setNameprtid(orders.getNameprtid());
        orderForm.setTrack(orders.getTrack());
        orderForm.setStatusid(orders.getStatusid());
        orderForm.setStaffid(orders.getStaffid());
        return orderForm;
    }
}
