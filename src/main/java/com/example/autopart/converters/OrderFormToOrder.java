package com.example.autopart.converters;

import com.example.autopart.commands.OrderForm;
import com.example.autopart.entity.Orders;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class OrderFormToOrder implements Converter<OrderForm, Orders> {
    @Override
    public Orders convert(OrderForm orderForm){
        Orders orders = new Orders();
        if (orderForm.getId() != null  && !StringUtils.isEmpty(orderForm.getId())) {
            orders.setId(orderForm.getId());
        }
        orders.setMarkid(orderForm.getMarkid());
        orders.setDetailid(orderForm.getDetailid());
        orders.setPriceid(orderForm.getPriceid());
        orders.setNameprtid(orderForm.getNameprtid());
        orders.setTrack(orderForm.getTrack());
        orders.setStatusid(orderForm.getStatusid());
        orders.setStaffid(orderForm.getStaffid());
        return orders;
    }


}
