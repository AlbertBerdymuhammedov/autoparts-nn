package com.example.autopart.service;

import com.example.autopart.entity.Markautos;
import com.example.autopart.repository.MarkRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MarkServiceImpl implements MarkService {

    private MarkRepo markRepo;

    @Autowired
    public MarkServiceImpl(MarkRepo markRepo){
        this.markRepo = markRepo;
    }

    @Override
    public List<Markautos> listAll() {
        List<Markautos> markautos = new ArrayList<>();
        markRepo.findAll().forEach(markautos::add);
        return markautos;
    }
    @Override
    public Markautos getById(Long id) {
        return markRepo.findById(id).orElse(null);
    }

}
