package com.example.autopart.service;


import com.example.autopart.commands.StaffForm;
import com.example.autopart.entity.Staffs;

import java.util.List;

public interface StaffService {
    List<Staffs> listAll();
    Staffs getById(Long id);
    Staffs saveOrUpdate(Staffs staffs);
    void delete(Long id);
    Staffs saveOrUpdateStaffForm(StaffForm staffForm);
}
