package com.example.autopart.service;

import com.example.autopart.entity.Details;

import java.util.List;

public interface DetailService {
    List<Details> listAll();
    Details getById(Long id);
}
