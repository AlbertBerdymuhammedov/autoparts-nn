package com.example.autopart.service;

import com.example.autopart.commands.CustomerForm;
import com.example.autopart.converters.CustomerFormToCustomer;
import com.example.autopart.entity.Customers;
import com.example.autopart.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerFormToCustomer customerFormToCustomer;
    private CustomerRepo customerRepo;

    @Autowired
    public CustomerServiceImpl(CustomerRepo customerRepo, CustomerFormToCustomer customerFormToCustomer){
        this.customerRepo = customerRepo;
        this.customerFormToCustomer = customerFormToCustomer;
    }

    @Override
    public List<Customers> listAll() {
        List<Customers> customers = new ArrayList<>();
        customerRepo.findAll().forEach(customers::add);
        return customers;
    }

    @Override
    public Customers getById(Long id) {
        return customerRepo.findById(id).orElse(null);
    }

    @Override
    public Customers saveOrUpdate(Customers customers) {
        customerRepo.save(customers);
        System.out.println("Save Customer Id: " + customers);
        return customers;
    }

    @Override
    public void delete(Long id) {
        customerRepo.deleteById(id);
        System.out.println("Delete Customer Id: " + id);
    }

    @Override
    public Customers saveOrUpdateCustomerForm(CustomerForm customerForm) {
        Customers savedCustomer = saveOrUpdate(customerFormToCustomer.convert(customerForm));

        System.out.println("Saved Customer Id: " + savedCustomer.getId());
        return savedCustomer;
    }
}
