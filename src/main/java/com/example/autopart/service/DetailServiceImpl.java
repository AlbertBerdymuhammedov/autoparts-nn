package com.example.autopart.service;

import com.example.autopart.entity.Details;
import com.example.autopart.entity.Markautos;
import com.example.autopart.repository.DetailRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DetailServiceImpl implements DetailService {

    private DetailRepo detailRepo;

    @Autowired
    public DetailServiceImpl(DetailRepo detailRepo){
        this.detailRepo = detailRepo;
    }

    @Override
    public List<Details> listAll() {
        List<Details> details = new ArrayList<>();
        detailRepo.findAll().forEach(details::add);
        return details;
    }

    @Override
    public Details getById(Long id) {
        return detailRepo.findById(id).orElse(null);
    }
}
