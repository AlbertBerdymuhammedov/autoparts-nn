package com.example.autopart.service;

import com.example.autopart.entity.Nameparts;

import java.util.List;

public interface NameprtService {
    List<Nameparts> listAll();
    Nameparts getById(Long id);
}
