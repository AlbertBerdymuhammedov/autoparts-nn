package com.example.autopart.service;

import com.example.autopart.entity.Staffs;
import com.example.autopart.entity.Status;
import com.example.autopart.repository.StatusRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StatusServiceImpl implements StatusService {

    private StatusRepo statusRepo;

    @Autowired
    public StatusServiceImpl(StatusRepo statusRepo){
        this.statusRepo= statusRepo;
    }

    @Override
    public List<Status> listAll() {
        List<Status> status = new ArrayList<>();
        statusRepo.findAll().forEach(status::add);
        return status;
    }
}
