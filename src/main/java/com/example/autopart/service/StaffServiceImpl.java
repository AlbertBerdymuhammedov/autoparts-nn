package com.example.autopart.service;

import com.example.autopart.commands.StaffForm;
import com.example.autopart.converters.StaffFormToStaff;
import com.example.autopart.entity.Staffs;
import com.example.autopart.repository.StaffRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StaffServiceImpl implements StaffService {

    private StaffFormToStaff staffFormToStaff;
    private StaffRepo staffRepo;

    @Autowired
    public StaffServiceImpl(StaffRepo staffRepo,StaffFormToStaff staffFormToStaff){
        this.staffRepo = staffRepo;
        this.staffFormToStaff = staffFormToStaff;
    }

    @Override
    public List<Staffs> listAll() {
        List<Staffs> staffs = new ArrayList<>();
        staffRepo.findAll().forEach(staffs::add);
        return staffs;
    }

    @Override
    public Staffs getById(Long id) {
        return staffRepo.findById(id).orElse(null);
    }

    @Override
    public Staffs saveOrUpdate(Staffs staffs) {
        staffRepo.save(staffs);
        System.out.println("Save Staff Id: " + staffs);
        return staffs;
    }

    @Override
    public void delete(Long id) {
        staffRepo.deleteById(id);
        System.out.println("Delete Staff Id: " + id);
    }

    @Override
    public Staffs saveOrUpdateStaffForm(StaffForm staffForm) {
        Staffs savedStaff = saveOrUpdate(staffFormToStaff.convert(staffForm));

        System.out.println("Saved Staff Id: " + savedStaff.getId());
        return savedStaff;
    }
}
