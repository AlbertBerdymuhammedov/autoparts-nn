package com.example.autopart.service;

import com.example.autopart.entity.Markautos;

import java.util.List;

public interface MarkService {
    List<Markautos> listAll();
    Markautos getById(Long id);
}
