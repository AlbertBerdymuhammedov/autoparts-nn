package com.example.autopart.service;


import com.example.autopart.commands.OrderForm;
import com.example.autopart.entity.Orders;

import java.util.List;

public interface OrderService {
    List<Orders> listAll();
    Orders getById(Long id);
    Orders saveOrUpdate(Orders orders);
    void delete(Long id);
    Orders saveOrUpdateOrderForm(OrderForm orderForm);
    Iterable<Orders> listByTrack(String track);
}
