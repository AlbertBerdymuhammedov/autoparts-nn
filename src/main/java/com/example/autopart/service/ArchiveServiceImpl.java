package com.example.autopart.service;

import com.example.autopart.commands.ArchiveForm;
import com.example.autopart.converters.ArchiveFormToArchive;
import com.example.autopart.entity.Archives;
import com.example.autopart.repository.ArchiveRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ArchiveServiceImpl implements ArchiveService {

    private ArchiveRepo archiveRepo;
    private ArchiveFormToArchive archiveFormToArchive;

    @Autowired
    public ArchiveServiceImpl(ArchiveRepo archiveRepo, ArchiveFormToArchive archiveFormToArchive){
        this.archiveRepo = archiveRepo;
        this.archiveFormToArchive = archiveFormToArchive;
    }

    @Override
    public List<Archives> listAll() {
        List<Archives> archives = new ArrayList<>();
        archiveRepo.findAll().forEach(archives::add);
        return archives;
    }

    @Override
    public Archives getById(Long id) {
        return archiveRepo.findById(id).orElse(null);
    }

    @Override
    public Archives saveOrUpdate(Archives archives) {
        archiveRepo.save(archives);
        System.out.println("Save Archive Id: " + archives);
        return archives;
    }

    @Override
    public void delete(Long id) {
        archiveRepo.deleteById(id);
        System.out.println("Delete Archive Id: " + id);
    }

    @Override
    public Archives saveOrUpdateArchiveForm(ArchiveForm archiveForm) {
        Archives savedArchive = saveOrUpdate(archiveFormToArchive.convert(archiveForm));

        System.out.println("Saved Archive Id: " + savedArchive.getId());
        return savedArchive;
    }

}
