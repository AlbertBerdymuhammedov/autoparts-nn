package com.example.autopart.service;

import com.example.autopart.entity.Nameparts;
import com.example.autopart.repository.NameprtRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NameprtServiceImpl implements NameprtService {

    private NameprtRepo nameprtRepo;

    @Autowired
    public NameprtServiceImpl(NameprtRepo nameprtRepo){
        this.nameprtRepo = nameprtRepo;
    }

    @Override
    public List<Nameparts> listAll() {
        List<Nameparts> nameparts = new ArrayList<>();
        nameprtRepo.findAll().forEach(nameparts::add);
        return nameparts;
    }

    @Override
    public Nameparts getById(Long id) {
        return nameprtRepo.findById(id).orElse(null);
    }

}
