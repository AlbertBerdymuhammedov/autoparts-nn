package com.example.autopart.service;

import com.example.autopart.commands.OrderForm;
import com.example.autopart.converters.OrderFormToOrder;
import com.example.autopart.entity.Orders;
import com.example.autopart.repository.OrderRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    private OrderFormToOrder orderFormToOrder;
    private OrderRepo orderRepo;

    @Autowired
    public OrderServiceImpl(OrderRepo orderRepo, OrderFormToOrder orderFormToOrder){
        this.orderRepo = orderRepo;
        this.orderFormToOrder = orderFormToOrder;
    }

    @Override
    public List<Orders> listAll() {
        List<Orders> orders = new ArrayList<>();
        orderRepo.findAll().forEach(orders::add);
        return orders;
    }

    @Override
    public Orders getById(Long id) {
        return orderRepo.findById(id).orElse(null);
    }

    @Override
    public Orders saveOrUpdate(Orders orders) {
        orderRepo.save(orders);
        System.out.println("Save Order Id: " + orders);
        return orders;
    }

    @Override
    public void delete(Long id) {
        orderRepo.deleteById(id);
        System.out.println("Delete Order Id: " + id);
    }

    @Override
    public Orders saveOrUpdateOrderForm(OrderForm orderForm) {
        Orders savedOrder = saveOrUpdate(orderFormToOrder.convert(orderForm));

        System.out.println("Saved Order Id: " + savedOrder.getId());
        return savedOrder;
    }

    @Override
    public Iterable<Orders> listByTrack(String track){
        return orderRepo.findByTrack(track);
    }
}
