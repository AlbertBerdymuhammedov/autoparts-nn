package com.example.autopart.service;

import com.example.autopart.commands.ArchiveForm;
import com.example.autopart.entity.Archives;

import java.util.List;

public interface ArchiveService {
    List<Archives> listAll();
    Archives getById(Long id);
    Archives saveOrUpdate(Archives archives);
    void delete(Long id);
    Archives saveOrUpdateArchiveForm(ArchiveForm archiveForm);
}
