package com.example.autopart.service;

import com.example.autopart.entity.Status;

import java.util.List;

public interface StatusService {
    List<Status> listAll();
}
