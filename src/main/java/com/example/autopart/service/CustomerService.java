package com.example.autopart.service;

import com.example.autopart.commands.CustomerForm;

import com.example.autopart.entity.Customers;

import java.util.List;

public interface CustomerService {
    List<Customers> listAll();
    Customers getById(Long id);
    Customers saveOrUpdate(Customers customers);
    void delete(Long id);
    Customers saveOrUpdateCustomerForm(CustomerForm customerForm);
}
